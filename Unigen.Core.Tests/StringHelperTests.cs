﻿using Unigen.Core.Helpers;
using Xunit;

namespace Unigen.Core.Tests
{
    public class StringHelperTests
    {
        [Fact]
        public void GetLastPartOfTest()
        {
            Assert.Equal("Company", "definitions.Company".GetLastPartOf("definitions."));
            Assert.Equal("Company", "definitions.Company".GetLastPartOf("."));
        }

        [Fact]
        public void GetClassNameTest()
        {
            Assert.Equal("ClientClientcodeBacsEntityCallbackPostAsync", "/client/{clientCode}/BACS/{entity}/callback".GetMethodName("POST", "Async"));
        }

    }
}
