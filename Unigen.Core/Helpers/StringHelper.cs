﻿namespace Unigen.Core.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        /// Returns everything after the found "search".
        /// </summary>
        public static string GetLastPartOf(this string value, string search)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(search))
                return value;

            var index = value.LastIndexOf(search); // definitions.Company
            if (index >= 0)
            {
                var start = index + search.Length;
                var length = value.Length - start;
                return value.Substring(start, length);
            }

            return value;
        }

        /// <summary>
        /// Gets the http path and http method and produces the safe method name
        /// </summary>
        public static string GetMethodName(this string path, string method, string? postfix)
        {
            var parts = path.Replace("{", "").Replace("}", "").Split("/", StringSplitOptions.RemoveEmptyEntries);
            var result = string.Join("", parts.Select(x => x.ToLowerInvariant().WithCapital())) + method.ToLowerInvariant().WithCapital() + postfix?.ToLowerInvariant().WithCapital();
            return result;
        }

        /// <summary>
        /// Makes the first letter capital
        /// </summary>
        public static string WithCapital(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return str.Substring(0, 1).ToUpper() + str.Substring(1);
        }
    }
}
