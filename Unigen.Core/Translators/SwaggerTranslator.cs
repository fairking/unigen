﻿using Newtonsoft.Json.Linq;
using Unigen.Core.Helpers;
using Unigen.Core.Models;

namespace Unigen.Core.Translators
{
    public class SwaggerTranslator
    {
        private readonly JObject _root;

        public SwaggerTranslator(JObject root)
        {
            _root = root ?? throw new ArgumentNullException(nameof(root));
        }

        public ApiSchema GetSchema(string version, CancellationToken cancellationToken = default)
        {
            var models = new List<ApiModel>();

            // Load models
            {
                var definitions = _root["definitions"];

                if (definitions != null)
                {
                    foreach (var def in definitions)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        var model = _GetModel(def.Value<JProperty>(), cancellationToken);
                        models.Add(model);
                    }
                }
            }

            var methods = new List<ApiMethod>();

            // Load methods
            {
                var paths = _root["paths"];

                if (paths != null)
                {
                    foreach (var path in paths)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        var path_prop = path.Value<JProperty>();
                        foreach (var meth in path_prop.Value.Children<JProperty>())
                        {
                            cancellationToken.ThrowIfCancellationRequested();
                            var method = _GetMethod(meth.Value<JProperty>(), path_prop.Name);
                            methods.Add(method);
                            models.AddRange(method.Results.Where(x => x.Model != null).Select(x => x.Model));
                        }
                    }
                }
            }

            // Return
            return new ApiSchema()
            {
                Name = _root["info"]?["name"]?.ToString(),
                Summary = _root["info"]?["description"]?.ToString(),
                Models = models.ToDictionary(x => x.Name, x => x),
                Methods = methods.ToDictionary(x => x.Name, x => x),
            };
        }

        #region Private Methods

        private ApiMethod _GetMethod(JProperty property, string path, CancellationToken cancellationToken = default)
        {
            var method = new ApiMethod()
            {
                Name = path.GetMethodName(property.Name, null),
                Path = path,
                Method = property.Name,
                Summary = property.Value.Value<string>("description"),
            };

            // Parameters
            {
                var apiParameters = new List<ApiParameter>();

                var parameters = property.Value.Value<JArray>("parameters");

                if (parameters != null)
                {
                    foreach (JObject par in parameters)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        var result = _GetParameter(par);
                        apiParameters.Add(result);
                    }
                }

                method.Parameters = apiParameters.ToArray();
            }

            // Results
            {
                var results = new List<ApiResult>();

                var responses = property.Value["responses"];

                if (responses != null)
                {
                    foreach (var response in responses)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        var result = _GetResponse(response.Value<JProperty>(), method.Name);
                        results.Add(result);
                    }
                }

                method.Results = results.ToArray();
            }

            return method;
        }

        private ApiParameter _GetParameter(JObject property)
        {
            var parameter = new ApiParameter()
            {
                Name = property["name"]?.ToString(),
                Summary = property["description"]?.ToString(),
                Type = property["schema"]?["$ref"]?.ToString() ?? property["schema"]?["type"]?.ToString() ?? property["type"]?.ToString(),
                ParameterType = _GetParameterType(property["in"]?.ToString()),
            };

            return parameter;
        }

        private ApiParameterType _GetParameterType(string type)
        {
            if (string.IsNullOrWhiteSpace(type))
                throw new ArgumentNullException(nameof(type));

            switch (type)
            {
                case "path":
                    return ApiParameterType.Path;
                case "query":
                    return ApiParameterType.Query;
                case "body":
                    return ApiParameterType.Body;
                case "header":
                    return ApiParameterType.Header;
                default:
                    throw new NotImplementedException($"The method parameter type '{type}' is not implemented.");
            }
        }

        private ApiResult _GetResponse(JProperty property, string modelName)
        {
            var result = new ApiResult()
            {
                Name = property.Name,
                Summary = property.Value.Value<string>("description"),
            };

            var schema_prop = property.Value["schema"];

            if (schema_prop != null)
            {
                result.Model = _GetModel(new JProperty(modelName + result.Name, schema_prop));
                result.Type = result.Model.Type;
                if (!result.Model.Properties.Any())
                    result.Model = null;
            }

            return result;
        }

        private ApiModel _GetModel(JProperty property, CancellationToken cancellationToken = default)
        {
            if (property == null)
                throw new ArgumentNullException(nameof(property));

            var model = new ApiModel()
            {
                Name = property.Name,
                Summary = property.Value.Value<string>("description"),
                Type = property.Value.Value<string>("$ref") ?? property.Value.Value<string>("type") ?? "object",
            };

            var props = new List<ApiProperty>();

            var jProps = property.Value["properties"];

            if (jProps != null)
            {
                foreach (var prop in jProps)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    var modelProp = _GetProperty(prop.Value<JProperty>());
                    props.Add(modelProp);
                }
            }

            model.Properties = props.ToArray();

            return model;
        }

        private ApiProperty _GetProperty(JProperty property)
        {
            var modelProp = new ApiProperty()
            {
                Name = property.Name,
                Summary = property.Value.Value<string>("description"),
                Type = property.Value.Value<string>("$ref") ?? property.Value.Value<string>("type"),
            };

            if (modelProp.Type == "array")
            {
                modelProp.ArrayType = property.Value.Value<JObject>("items")["$ref"]?.ToString() ?? property.Value.Value<JObject>("items")["type"]?.ToString() ?? "object";
            }

            return modelProp;
        }

        #endregion Private Methods
    }
}
