﻿using Newtonsoft.Json.Linq;
using Unigen.Core.Models;

namespace Unigen.Core.Translators
{
    public class OpenapiTranslator
    {
        private readonly JObject _root;

        public OpenapiTranslator(JObject root)
        {
            _root = root ?? throw new ArgumentNullException(nameof(root));
        }

        public ApiSchema GetSchema(string version, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        #region Private Methods

        #endregion Private Methods
    }
}
