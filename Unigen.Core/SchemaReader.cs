﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Unigen.Core.Models;
using Unigen.Core.Translators;

namespace Unigen.Core
{
    public class SchemaReader
    {
        private readonly string _path;

        public JObject? Schema { get; protected set; }

        public ApiSchema ApiSchema { get; protected set; }

        public SchemaReader(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));

            _path = path;
        }

        public SchemaReader ReadSchema(CancellationToken cancellationToken = default)
        {
            _ReadSchemaFile(cancellationToken);

            _TranslateSchema(cancellationToken);

            return this;
        }

        #region Private Methods

        private void _ReadSchemaFile(CancellationToken cancellationToken = default)
        {
            string stringSchema;

            if (_path.StartsWith("http"))
            {
                using (var client = new HttpClient())
                {
                    stringSchema = client.GetStringAsync(_path, cancellationToken).GetAwaiter().GetResult();
                    File.WriteAllTextAsync("schema.json", stringSchema, cancellationToken);
                }
            }
            else
            {
                stringSchema = File.ReadAllTextAsync(_path, cancellationToken).GetAwaiter().GetResult();
            }

            if (string.IsNullOrWhiteSpace(stringSchema))
                throw new Exception("Schema has no content.");

            Schema = JsonConvert.DeserializeObject<JObject>(stringSchema);
        }

        private void _TranslateSchema(CancellationToken cancellationToken = default)
        {
            if (Schema == null)
                throw new ArgumentNullException(nameof(Schema));

            if (Schema.TryGetValue("swagger", out var swaggerVersion))
            {
                ApiSchema = new SwaggerTranslator(Schema).GetSchema(swaggerVersion.Value<string>(), cancellationToken);
            }
            else if (Schema.TryGetValue("openapi", out var openapiVersion))
            {
                ApiSchema = new OpenapiTranslator(Schema).GetSchema(openapiVersion.Value<string>(), cancellationToken);
            }
            else
            {
                throw new Exception("Unknown api version.");
            }
        }

        #endregion Private Methods
    }
}