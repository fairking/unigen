﻿namespace Unigen.Core.Models
{
    public class ApiModel : ApiObject
    {
        public string Type { get; set; }

        public ApiProperty[] Properties { get; set; }
    }
}
