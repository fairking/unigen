﻿namespace Unigen.Core.Models
{
    public class ApiSchema : ApiObject
    {
        public IDictionary<string, ApiModel> Models { get; set; }
        public IDictionary<string, ApiMethod> Methods { get; set; }
    }
}
