﻿namespace Unigen.Core.Models
{
    public class ApiParameter : ApiObject
    {
        public string Type { get; set; }

        public ApiParameterType ParameterType { get; set; }
    }

    public enum ApiParameterType
    {
        Query,
        Path,
        Header,
        Body,
    }
}
