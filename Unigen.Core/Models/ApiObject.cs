﻿namespace Unigen.Core.Models
{
    public abstract class ApiObject
    {
        public string Name { get; set; }

        public string Summary { get; set; }
    }
}
