﻿namespace Unigen.Core.Models
{
    public class ApiProperty : ApiObject
    {
        public string Type { get; set; }

        public string ArrayType { get; set; }
    }
}
