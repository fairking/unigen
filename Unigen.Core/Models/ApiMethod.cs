﻿namespace Unigen.Core.Models
{
    public class ApiMethod : ApiObject
    {
        public string Path { get; set; }

        public string Method { get; set; }

        public ApiParameter[] Parameters { get; set; }

        public ApiResult[] Results { get; set; }
    }
}
