﻿using System.Text;
using Unigen.Core.Helpers;
using Unigen.Core.Models;

namespace Unigen.CSharp
{
    public class ClientWriter
    {
        private readonly ApiSchema _schema;

        public ClientWriter(ApiSchema schema)
        {
            _schema = schema ?? throw new ArgumentNullException(nameof(schema));
        }

        public void Write(string path, string ns, string title, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(ns))
                ns = "MyProject";

            if (string.IsNullOrWhiteSpace(title))
                title = "ApiClient";

            if (string.IsNullOrWhiteSpace(path))
                path = title + ".cs";

            var result = new StringBuilder();

            // Usings
            result.AppendLine("using System;");
            result.AppendLine("using Newtonsoft.Json;");
            result.AppendLine("using System.Net;");
            result.AppendLine("using System.Net.Http;");
            result.AppendLine("using System.Threading;");
            result.AppendLine("using System.Threading.Tasks;");
            result.AppendLine("using System.Text;");
            result.AppendLine("");

            // Begin Namespace
            result.AppendLine($"namespace {ns}");
            result.AppendLine("{");

            // Begin Class
            _TryWriteSummary(result, _schema.Name, "    ");
            //_TryWriteSummary(result, _schema.Summary, "    "); // TODO: !!! Description can be too long for class summary (trancate??)
            result.AppendLine($"    public class {title} : HttpClient");
            result.AppendLine("    {");
            result.AppendLine($"        public {title}(string baseUrl) : base()");
            result.AppendLine("        {");
            result.AppendLine("            BaseAddress = new Uri(baseUrl, UriKind.Absolute);");
            result.AppendLine("        ");
            result.AppendLine("            // Only json is supported at the moment");
            result.AppendLine("            DefaultRequestHeaders.Add(\"Accept\", \"application/json\");");
            result.AppendLine("        }");

            // Methods
            foreach (var method in _schema.Methods)
            {
                cancellationToken.ThrowIfCancellationRequested();
                _WriteMethod(result, method.Value);
            }

            // End Class
            result.AppendLine("    }");

            // Models
            foreach (var model in _schema.Models)
            {
                cancellationToken.ThrowIfCancellationRequested();
                _WriteModel(result, model.Value);
            }

            // ApiResponse
            _WriteApiResponseClass(result);

            // ApiException
            _WriteApiExceptionClass(result);

            // End Namespace
            result.AppendLine("}");

            File.WriteAllTextAsync(path, result.ToString(), cancellationToken).GetAwaiter().GetResult();
        }

        #region Private Methods

        private void _WriteApiExceptionClass(StringBuilder sb)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            sb.AppendLine("");
            sb.AppendLine("    public class ApiException : Exception");
            sb.AppendLine("    {");
            sb.AppendLine("        public ApiException(ApiResponse response) : base($\"The server responded with status code '{response?.StatusCode}'.\")");
            sb.AppendLine("        {");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine("        public ApiResponse Response { get; protected set; }");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    public class ApiException<TError> : ApiException");
            sb.AppendLine("    {");
            sb.AppendLine("        public ApiException(ApiResponse<TError> response) : base(response)");
            sb.AppendLine("        {");
            sb.AppendLine("            Response = response ?? throw new ArgumentNullException(nameof(response));");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine("        public new ApiResponse<TError> Response { get; protected set; }");
            sb.AppendLine("    }");
        }

        private void _WriteApiResponseClass(StringBuilder sb)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            sb.AppendLine("");
            sb.AppendLine("    public class ApiResponse");
            sb.AppendLine("    {");
            sb.AppendLine("        public ApiResponse(Uri url, HttpMethod method, HttpStatusCode statusCode, string response)");
            sb.AppendLine("        {");
            sb.AppendLine("            Url = url;");
            sb.AppendLine("            Method = method;");
            sb.AppendLine("            StatusCode = statusCode;");
            sb.AppendLine("            Response = response;");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        public Uri Url { get; protected set; }");
            sb.AppendLine("        ");
            sb.AppendLine("        public HttpMethod Method { get; protected set; }");
            sb.AppendLine("        ");
            sb.AppendLine("        public HttpStatusCode StatusCode { get; protected set; }");
            sb.AppendLine("        ");
            sb.AppendLine("        public bool IsOk => StatusCode == HttpStatusCode.OK;");
            sb.AppendLine("        ");
            sb.AppendLine("        public string Response { get; protected set; }");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    public class ApiResponse<TResult> : ApiResponse");
            sb.AppendLine("    {");
            sb.AppendLine("        public ApiResponse(Uri url, HttpMethod method, HttpStatusCode statusCode, string response) : base(url, method, statusCode, response)");
            sb.AppendLine("        {");
            sb.AppendLine("            Data = JsonConvert.DeserializeObject<TResult>(Response);");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        public TResult Data { get; protected set; }");
            sb.AppendLine("    }");
        }

        private void _WriteMethod(StringBuilder sb, ApiMethod method)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (method == null)
                throw new ArgumentNullException(nameof(method));

            var returnType = _GetReturnType(method.Results.SingleOrDefault(x => x.Name == "200"));
            var httpMethod = method.Method.ToLower().WithCapital();

            sb.AppendLine("");
            _TryWriteMethodSummary(sb, method);
            sb.AppendLine($"        public async Task<ApiResponse<{returnType}>> {method.Name + "Async"}({_WriteQueries(method.Parameters)}CancellationToken cancellationToken = default)");
            sb.AppendLine("        {");
            sb.AppendLine($"            var _url = {_WriteUrl(method.Path, method.Parameters)};");
            sb.AppendLine($"            var _request = new HttpRequestMessage(new HttpMethod(\"{httpMethod}\"), _url);");
            _WriteHeaders(sb, method.Parameters);
            _WriteBody(sb, method.Parameters);
            sb.AppendLine("            var _response = await SendAsync(_request, HttpCompletionOption.ResponseContentRead, cancellationToken);");
            sb.AppendLine("            var _responseContent = await _response.Content.ReadAsStringAsync();");
            _WriteResults(sb, method);
            sb.AppendLine("        }");
        }

        private void _TryWriteMethodSummary(StringBuilder sb, ApiMethod method)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            _TryWriteSummary(sb, method.Summary, "        ");
            foreach (var result in method.Results.Where(x => x.Name != "200"))
            {
                sb.AppendLine($"        /// <exception cref=\"ApiException<{_GetReturnType(result)}>\">The server responded with status code '{result.Name}'. {result.Summary}</exception>");
            }
            sb.AppendLine("        /// <exception cref=\"ApiException<object>\">The server responded with status code 'XXX'.</exception>");
        }

        private string _GetReturnType(ApiResult result)
        {
            if (result == null)
                throw new ArgumentNullException(nameof(result));

            var returnType = _GetType(result?.Model?.Name ?? result?.Type ?? "object", null, result.Name);
            return returnType;
        }

        private void _WriteResults(StringBuilder sb, ApiMethod method)
        {
            if (method == null)
                throw new ArgumentNullException(nameof(method));

            var ok = method.Results.SingleOrDefault(x => x.Name == "200");

            sb.AppendLine("            switch ((int)_response.StatusCode)");
            sb.AppendLine("            {");
            if (ok == null)
            {
                sb.AppendLine($"                case 200:");
                sb.AppendLine($"                    return new ApiResponse<object>(_request.RequestUri, _request.Method, _response.StatusCode, _responseContent);");
            }
            foreach (var result in method.Results)
            {
                sb.AppendLine($"                case {result.Name}:");
                if (result.Name != "200")
                    sb.AppendLine($"                    throw new ApiException<{_GetReturnType(result)}>(new ApiResponse<{_GetReturnType(result)}>(_request.RequestUri, _request.Method, _response.StatusCode, _responseContent));");
                else
                    sb.AppendLine($"                    return new ApiResponse<{_GetReturnType(result)}>(_request.RequestUri, _request.Method, _response.StatusCode, _responseContent);");
            }
            sb.AppendLine("                default:");
            sb.AppendLine($"                    throw new ApiException<object>(new ApiResponse<object>(_request.RequestUri, _request.Method, _response.StatusCode, _responseContent));");
            sb.AppendLine("            }");
        }

        private string? _WriteQueries(ApiParameter[] parameters)
        {
            if (parameters == null || !parameters.Any())
                return null;

            var result = "";

            foreach (var parameter in parameters)
            {
                result += $"{_GetType(parameter.Type, null, parameter.Name)} {parameter.Name}, ";
            }

            return result;
        }

        private void _WriteModel(StringBuilder sb, ApiModel model)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (model == null)
                throw new ArgumentNullException(nameof(model));

            sb.AppendLine("");
            _TryWriteSummary(sb, model.Summary, "    ");
            sb.AppendLine($"    public class {model.Name}");
            sb.AppendLine("    {");
            foreach (var property in model.Properties)
                _WriteProperty(sb, property, "        ");
            sb.AppendLine("    }");
        }

        private void _WriteProperty(StringBuilder sb, ApiProperty property, string indent = "")
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (property == null)
                throw new ArgumentNullException(nameof(property));

            sb.AppendLine("");
            _TryWriteSummary(sb, property.Summary, "        ");
            sb.AppendLine($"        public {_GetType(property.Type, property.ArrayType, property.Name)} {property.Name} {{ get; set; }}");
        }

        private void _TryWriteSummary(StringBuilder sb, string summary, string intend = "")
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (!string.IsNullOrWhiteSpace(summary))
            {
                sb.AppendLine($"{intend}/// <summary>");
                foreach (var line in summary.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries))
                    sb.AppendLine($"{intend}/// {line}");
                sb.AppendLine($"{intend}/// </summary>");
            }
        }

        private string _GetType(string apiType, string arrayType, string resultName)
        {
            if (string.IsNullOrWhiteSpace(apiType))
                throw new Exception($"The method or model or property {resultName} has missing type provided.");

            if (apiType.Contains("/"))
                return apiType.GetLastPartOf("/");

            switch (apiType)
            {
                case "number":
                    return "decimal";
                case "integer":
                    return "int";
                case "boolean":
                    return "bool";
                case "array":
                    return arrayType != null ? arrayType.GetLastPartOf("/") + "[]" : "string[]";
                default:
                    return apiType;
            }
        }

        private string _WriteUrl(string path, ApiParameter[] apiParameters)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException(nameof(path));

            var result = $"$\"{path}";

            if (apiParameters != null && apiParameters.Any(x => x.ParameterType == ApiParameterType.Query))
            {
                result += "?" + string.Join("&", 
                    apiParameters
                        .Where(x => x.ParameterType == ApiParameterType.Query)
                        .Select(x => x.Name + $"={{{x.Name}}}")
                );
            }

            result += "\"";

            return result;
        }

        private void _WriteHeaders(StringBuilder sb, ApiParameter[] apiParameters)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (apiParameters == null || !apiParameters.Any(x => x.ParameterType == ApiParameterType.Header))
                return;

            foreach (var header in apiParameters.Where(x => x.ParameterType == ApiParameterType.Header))
            {
                sb.AppendLine($"            _request.Headers.Add(\"{header.Name}\", $\"{{{header.Name}}}\");");
            }
        }

        private void _WriteBody(StringBuilder sb, ApiParameter[] apiParameters)
        {
            if (sb == null)
                throw new ArgumentNullException(nameof(sb));

            if (apiParameters == null || !apiParameters.Any(x => x.ParameterType == ApiParameterType.Body))
                return;

            foreach (var header in apiParameters.Where(x => x.ParameterType == ApiParameterType.Body))
            {
                sb.AppendLine($"            if (body != null) _request.Content = new StringContent(JsonConvert.SerializeObject({header.Name}), Encoding.UTF8, \"application/json\");");
            }
        }

        #endregion Private Methods
    }
}
