﻿using CommandLine;
using CommandLine.Text;
using Unigen.Core;
using Unigen.CSharp;

namespace Unigen.Cli
{
    internal class Program
    {
        public class Options
        {
            [Option('c', "client", Required = true, HelpText = "Which client needs to be generated: cs, js, ts")]
            public string? Client { get; set; }

            [Option('s', "schema", Required = true, HelpText = "Url or path to the schema.json")]
            public string? Schema { get; set; }

            [Option('o', "output", Required = false, HelpText = "Path to the output client file. Empty path saves into current category.")]
            public string? Output { get; set; }

            [Option('n', "namespace", Required = false, HelpText = "Namespace of the class. Default: 'MyProject'.")]
            public string? Namespace { get; set; }

            [Option('t', "title", Required = false, HelpText = "Title of the class. Default: 'ApiClient'.")]
            public string? Title { get; set; }

            [Usage]
            static IEnumerable<Example> Examples
            {
                get
                {
                    yield return new Example("Generates C# client from schema into ApiClient.cs in the current directory.", new Options { Client = "cs", Schema = "swagger.json" });
                    yield return new Example("Generates C# client from schema into ApiClient.cs in the current directory.", new Options { Client = "cs", Schema = "C:\\swagger.json", Output = "C:\\Code\\ApiClient.cs" });
                }
            }
        }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

            var arguments = Parser.Default.ParseArguments<Options>(args);

            arguments.WithParsed(o =>
            {
                var reader = new SchemaReader(o.Schema)
                    .ReadSchema(TokenSource.Token);

                switch (o.Client)
                {
                    case "cs":
                        new ClientWriter(reader.ApiSchema).Write(o.Output, o.Namespace, o.Title, TokenSource.Token);
                        break;
                    default:
                        throw new NotImplementedException($"The client '{o.Client}' is not implemented.");
                }
            });
        }

        private static CancellationTokenSource TokenSource = new CancellationTokenSource();

        private static void CurrentDomain_ProcessExit(object? sender, EventArgs e)
        {
            TokenSource.Cancel();
        }
    }
}